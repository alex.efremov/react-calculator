const numbers = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'];

const controls = ['.', 'c', 'del', '+', '-', '*', '/', '%'];

const findPoint = expr => expr.includes('.');

const checkForLegalSymbol = symbol => numbers.includes(symbol) || controls.includes(symbol);

const deleteSpaces = expr => expr.replace(/ /g,"");

const insertSpaces = expr => expr.replace(/\B(?=(\d{3})+(?!\d))/g, " ");

const processExpression = (expr, newSymbol='') => {
  let newExpr = expr + newSymbol;
  if (!findPoint(expr)) {
    newExpr = insertSpaces(deleteSpaces(newExpr));
  }
  return newExpr;
};

const insertOperator = (expr, operator) => {
  if (expr[expr.length-1] === operator || expr.length === 0) {
    return expr;
  }
  else {
    return expr + operator;
  }
};

const controlsActions = {
  '.': expr => findPoint(expr) ? expr : expr + '.',
  'c': () => '',
  'del': expr => processExpression(expr.slice(0,-1)),
  '*': expr => insertOperator(expr, '×')
};

const operators = {
  '+': {
    precedence: 1,
    action: (a, b) => a + b
  },
  '*': {
    precedence: 2,
    action: (a, b) => a * b
  }
};

const calculate = expr => {

};

export class Calculator {
  constructor(initialExpression = '') {
    this.expression = initialExpression;
    this.result = '';
  }

  get = () => ({
    expression: this.expression,
    result: this.result
  })

  set = (symbol='') => {
    if (checkForLegalSymbol(symbol)) {
      if (numbers.includes(symbol)) {
        this.expression = processExpression(this.expression, symbol);
      } else if (controls.includes(symbol)) {
        this.expression = controlsActions[symbol](this.expression);
      }
      this.result = calculate(this.expression);
    }
    return this;
  };
}
