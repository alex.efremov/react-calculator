import React from 'react';
import ReactDOM from 'react-dom';

import App from './components/App';
import { Calculator } from './utils';

const calculator = new Calculator();

ReactDOM.render(<App calculator={calculator} />, document.getElementById('root'));
