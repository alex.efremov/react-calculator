import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

//eslint-disable-next-line
const _Wrapper = ({ children, className }) => {
  const defaultSize = 100 / (React.Children.count(children) || 1);
  return (
    <div className={className} >
      {React.Children.map(children, child => {
        return React.cloneElement(child, {
          defaultSize
        });
      })}
    </div>
  );
};

_Wrapper.propTypes = {
  className: PropTypes.string
};

const Wrapper = styled(_Wrapper)`
  display: flex;
  flex-wrap: wrap;
  width: ${props => props.width || 'auto'};
  height: ${props => props.height || 'auto'};
`;

export default Wrapper;
