import Button from './Button';

const ControlButton = Button.extend`
  background-color: #f2f2f2;
  color: #4d4dff;
`;

export default ControlButton;
