import Button from './Button';

const InverseControlButton = Button.extend`
  background-color: #4d4dff;
  color: white;
  &:active {
    background-color: #1a1aff;
  }
`;

export default InverseControlButton;
