import { css } from 'styled-components';

export const flexBasis = css`
flex-basis: ${props => props.__size ? `${props.__size}%` :
    props.defaultSize ? `${props.defaultSize}%` : 'auto'}
`;
