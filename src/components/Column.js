import styled from 'styled-components';
import { flexBasis } from './styles';

const Column = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  ${flexBasis};
`;

export default Column;
