import React from 'react';
import PropTypes from 'prop-types';
import { injectGlobal } from 'styled-components';

import Display from './Display';
import Keyboard from './Keyboard';
import Wrapper from './Wrapper';

//eslint-disable-next-line
injectGlobal`
  @font-face {
    src: url('https://fonts.googleapis.com/css?family=Lato:300');
  }
  html {
    box-sizing: border-box;
  }
  *, *:before, *:after {
    box-sizing: inherit;
  }
  body {
    font-family: 'Lato', sans-serif;
    margin: 0;
    font-weight: 300;
  }
`;

class App extends React.Component {
  static propTypes = {
    calculator: PropTypes.object
  }

  constructor(props) {
    super();
    const state = props.calculator.get();
    this.state = {
      expr: state.expression,
      result: state.result
    };
  }

  handleOnClick = e => {
    const { calculator } = this.props;
    const newState = calculator.set(e.target.value).get();
    this.setState({
      expr: newState.expression,
      result: newState.result
    });
  };

  render() {
    return (
      <Wrapper
        height="100vh"
        width="100vw"
      >
        <Display
          exprValue={this.state.expr}
          height="25%"
          width="100%"
        />
        <Keyboard
          height="75%"
          onClick={this.handleOnClick}
          width="100%"
        />
      </Wrapper>
    );
  }
};

export default App;
