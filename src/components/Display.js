import React from 'react';
import PropTypes from 'prop-types';

import Wrapper from './Wrapper';
import Row from './Row';
import Output from './Output';

const Display = ({ caretPosition, exprValue, height, width, onCaretChange }) => {
  return (
    <Wrapper
      height={height}
      width={width}
    >
      <Row>
        <Output
          caretPosition={caretPosition}
          exprValue={exprValue}
          onCaretChange={onCaretChange}
        />
      </Row>
    </Wrapper>
  );
};

Display.propTypes = {
  caretPosition: PropTypes.number,
  exprValue: PropTypes.string,
  height: PropTypes.string.isRequired,
  onCaretChange: PropTypes.func,
  width: PropTypes.string.isRequired
};

export default Display;
