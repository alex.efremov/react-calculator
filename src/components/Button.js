import styled from 'styled-components';
import { flexBasis } from './styles';

const Button = styled.button`
  user-select: none;
  -webkit-touch-callout: none;
  display: flex;
  justify-content: center;
  -webkit-tap-highlight-color: rgba(255, 255, 255, 0);
  &::-moz-focus-inner {
    border: 0;
  }
  cursor: pointer;
  align-items: center;
  flex-grow: 1;
  font-size: 2rem;
  background-color: white;
  color: black;
  border: 1px solid #e6e6e6;
  outline: none;
  &:active {
    background-color: #e6e6e6;
  }
  ${flexBasis};
`;

export default Button;
