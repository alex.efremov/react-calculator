import React from 'react';
import PropTypes from 'prop-types';

import Button from './Button';
import ControlButton from './ControlButton';
import InverseControlButton from './InverseControlButton';
import Column from './Column';
import Wrapper from './Wrapper';

const Keyboard = ({ height, width, onClick }) => {
  return (
    <Wrapper
      height={height}
      width={width}
    >
      <Column onClick={onClick}>
        <ControlButton value='c'>
          c
        </ControlButton>
        <Button value='7'>
          7
        </Button>
        <Button value='4'>
          4
        </Button>
        <Button value='1'>
          1
        </Button>
        <Button value='%'>
          &#37;
        </Button>
      </Column>
      <Column onClick={onClick}>
        <ControlButton value='/'>
          &#247;
        </ControlButton>
        <Button value='8'>
          8
        </Button>
        <Button value='5'>
          5
        </Button>
        <Button value='2'>
          2
        </Button>
        <Button value='0'>
          0
        </Button>
      </Column>
      <Column onClick={onClick}>
        <ControlButton value='*'>
          &#215;
        </ControlButton>
        <Button value='9'>
          9
        </Button>
        <Button value='6'>
          6
        </Button>
        <Button value='3' >
          3
        </Button>
        <Button value='.'>
          .
        </Button>
      </Column>
      <Column onClick={onClick}>
        <ControlButton
          __size='20'
          value='del'
        >
          &#9003;
        </ControlButton>
        <ControlButton
          __size='20'
          value='-'
        >
          &#8722;
        </ControlButton>
        <ControlButton
          __size='20'
          value='+'
        >
          &#43;
        </ControlButton>
        <InverseControlButton
          __size='40'
          value='='
        >
          &#61;
        </InverseControlButton>
      </Column>
    </Wrapper>
  );
};

Keyboard.propTypes = {
  height: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  width: PropTypes.string.isRequired
};

export default Keyboard;
