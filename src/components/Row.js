import styled from 'styled-components';
import { flexBasis } from './styles';

const Row = styled.div`
  display: flex;
  flex-grow: 1;
  ${flexBasis};
`;

export default Row;
