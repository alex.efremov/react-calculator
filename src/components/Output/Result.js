import Expression from './Expression';

const Result = Expression.extend`
  font-size: 1.9rem;
`;

export default Result;
