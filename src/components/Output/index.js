import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Expression from './Expression';
import Result from './Result';

class Output extends React.Component {
  static propTypes = {
    className: PropTypes.string,
    exprValue: PropTypes.string,
    resultValue: PropTypes.string,
  }

  render() {
    const { className, exprValue, resultValue } = this.props;
    return (
      <div className={className}>
        <Expression
          innerRef={el => this.Expr = el}
          readOnly
          value={exprValue}
        />
        <Result
          readOnly
          value={resultValue}
        />
      </div>
    );
  }
}

export default styled(Output)`
  width: 100%;
`;
