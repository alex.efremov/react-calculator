import styled from 'styled-components';

const Expression = styled.input.attrs({
  type: 'text'
})`
  border: none;
  display: inline-block;
  position: relative;
  top: 50%;
  transform: translateY(-50%);
  padding-right: 10px;
  font-size: 2.5rem;
  outline: none;
  vertical-align:middle;
  text-align: right;
  width: 100%;
`;

export default Expression;
